
class CfgFile(object):
    def __init__(self):
        self.lines = []

    def set_param(self, name, value):
        new_line = '{0} = "{1}"'.format(name, value)
        for i, line in enumerate(self.lines):
            if line.startswith(name):
                self.lines[i] = new_line
                return True
        self.lines.append(new_line)
        return False

    def get_param(self, name):
        for line in self.lines:
            if line.startswith(name):
                return line.split()[-1][1:-1]

    def code_line(self, line):
        new_line = ''
        for i, char in enumerate(line):
            new_line += chr(255-ord(char))
        return new_line

    def load(self, filename):
        file = open(filename, 'r')
        self.lines = file.read().splitlines()
        for i, line in enumerate(self.lines):
            if i >= 3:
                self.lines[i] = self.code_line(line)

    def save(self, filename):
        file = open(filename, "w")
        for i, line in enumerate(self.lines):
            if i >= 3:
                line = self.code_line(line)
            file.write(line+'\n')
        file.close()

if __name__ == '__main__':
    cfg = CfgFile()
    cfg.load('system.cfg')
    for line in cfg.lines:
        print line
    #print cfg.set_param('g_chatlog', 1)
    #print cfg.get_param('e_maxdistance_ratio_entities')
    #cfg.save('system2.cfg')
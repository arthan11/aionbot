# -*- coding: utf-8 -*-
import sys
import datetime
from time import sleep
from autopy import bitmap, key, mouse, color
from aion import Aion
from databasenet import DatabaseNet


class Craft(object):
    def __init__(self):
        self.master_name = ''
        self.supplies_name = ''
        self.do_learn = False

    def get_supply_cords(self, name):
        idx = -1
        try:
            idx = self.supplies.index(name)
        except:
            pass
        if idx < 0:
            print 'Nie moge znalezc: {0}'.format(name)
            sys.exit()
        tab = idx / 10
        x = (idx % 10) % 2
        y = ((idx % 10) - x) / 2
        return x, y, tab


    def select_and_speak_to(self, name, selected=False):
        if name != '':
            if not selected:
                key.tap(key.K_RETURN)
                sleep(0.5)
                key.type_string('/select {0}'.format(name))
                key.tap(key.K_RETURN)
            key.tap('c')

    def speak_to_master(self, selected=False):
        #self.select_and_speak_to(self.master_name, selected)
        sleep(2)
        #print 'Ide do NPC: {0}'.format(self.master_name)
        key.tap('1')
        sleep(0.5)
        key.tap('c')

    def speak_to_supplies(self, selected=False):
        #self.select_and_speak_to(self.supplies_name, selected)
        sleep(0.5)
        #print 'Ide do NPC: {0}'.format(self.supplies_name)
        key.tap('2')
        sleep(0.5)
        key.tap('c')


    def learn(self):
        p = aion.wait_for_img('craft_button_learn_{0}.bmp'.format(self.craft), delay=1, print_msg=False)
        mouse.smooth_move(aion.x+ p[0] + 100, aion.y + p[1] + 10)
        mouse.click()
        sleep(2)
        #if not 'You cannot be promoted as your skill level is too low.'
        key.tap(key.K_RETURN)

    def work_done(self):
        print 'Oddaje wykonane zadanie'
        p = aion.wait_for_img('craft_button_learn_{0}.bmp'.format(self.craft), delay=1, print_msg=False)
        mouse.smooth_move(aion.x+ p[0] + 100, aion.y + p[1] + 30)
        mouse.click()
        sleep(2)
        mouse.smooth_move(aion.x+ p[0] + 100, aion.y + p[1] + self.work_quest_pos)
        mouse.click()
        sleep(2)
        mouse.smooth_move(aion.x+ p[0] + 170, aion.y + p[1] + 135)
        sleep(1)
        aion.chat.readlines()
        mouse.click()
        quest_complete = False
        while True:
            aion.chat.readlines()
            if not quest_complete:
                quest_complete = aion.chat.quest_complete()
            if quest_complete:
                print 'Zadanie zakonczone'
                break
            else:
                sleep(1)

    def select_buy(self):
        p = aion.wait_for_img('shop_button_buy_{0}.bmp'.format(self.craft), delay=1, print_msg=False)
        mouse.smooth_move(aion.x+ p[0] + 100, aion.y + p[1] + 10)
        mouse.click()
        sleep(2)

    def work(self):
        p = aion.wait_for_img('craft_button_learn_{0}.bmp'.format(self.craft), delay=1, print_msg=False)
        mouse.smooth_move(aion.x+ p[0] + 100, aion.y + p[1] + 30)
        mouse.click()
        sleep(2)
        mouse.smooth_move(aion.x+ p[0] + 100, aion.y + p[1] + self.work_quest_pos )
        mouse.click()
        sleep(2)
        mouse.smooth_move(aion.x+ p[0] + 170, aion.y + p[1] + 135)
        sleep(1)
        aion.chat.readlines()
        mouse.click()
        recipe_id = None
        acquired_items = []
        while True:
            sleep(1)
            aion.chat.readlines()
            if not recipe_id:
                recipe_id = aion.chat.learned_recipe()
            if not acquired_items:
                acquired_items =  acquired_items + aion.chat.acquired_items()
            if recipe_id and acquired_items:
                #print recipe_id, acquired_items
                break
        #recipe_id = '155004230'
        #acquired_items = ('182290229', '16')
        tip = db.get_tip('item', recipe_id)
        print 'Zadanie:', tip.item.title
        '''
        print 'Required Materials'
        print '-'*20
        for material in tip.materials:
            print '{0} - {1}'.format(material['item'].title, material['count'])
        '''
        to_do = 0
        m = tip.material_by_id(acquired_items[0][0])
        to_do = int(acquired_items[0][1]) / int(m['count'])
        print 'do wykonania:', to_do

        for item in acquired_items:
            m = tip.material_by_id(item[0])
            tip.materials.remove(m)
        for item in self.supplies_in_items:
            m = tip.material_by_title(item)
            if m:
                tip.materials.remove(m)

        in_suplies = False
        suplies_page = 0
        p = None

        for i, material in enumerate(tip.materials):
            if i == 0:
                print 'Ide na zakupy'
            id = material['item'].id
            title = material['item'].title
            to_buy = int(material['count']) * to_do
            print 'Kupuje: {0} x {1}'.format(to_buy, title)
            if not in_suplies:
                self.speak_to_supplies()
                self.select_buy()
                p = aion.wait_for_img('item_Cooking Enhancement Stone.bmp', delay=1, print_msg=False)
                in_suplies = True

            supply_cords = self.get_supply_cords(title)

            x = aion.x + p[0] + 50 + 130*supply_cords[0]
            y = aion.y + p[1] + 15 + 30*supply_cords[1]
            while supply_cords[2] != suplies_page:
                if suplies_page > supply_cords[2]:
                    mouse.smooth_move(aion.x + p[0] + 167, aion.y + p[1] - 8)
                    mouse.click()
                    suplies_page -= 1
                    sleep(2)
                else:
                    mouse.smooth_move(aion.x + p[0] + 222, aion.y + p[1] - 8)
                    mouse.click()
                    suplies_page += 1
                    sleep(2)

            mouse.smooth_move(x, y)
            aion.ToggleShift(True)
            sleep(0.5)
            mouse.click(mouse.RIGHT_BUTTON)
            sleep(0.5)
            aion.ToggleShift(False)
            sleep(2)
            key.type_string(str(to_buy))
            sleep(2)
            key.tap(key.K_RETURN)

            # exit shop
            if len(tip.materials) - 1 == i:
                mouse.smooth_move(aion.x + p[0] + 175, aion.y + p[1] + 305)
                sleep(0.2)
                mouse.click()
                sleep(1)
                mouse.smooth_move(aion.x + p[0] + 220, aion.y + p[1] + 305)
                sleep(0.2)
                mouse.click()

        print 'Ide wykonac zadanie'
        key.tap('3')
        sleep(0.5)
        key.tap('c')
        # select work order in Craft Tool window
        p = aion.wait_for_img('craft_skill_{0}.bmp'.format(self.craft), delay=1, print_msg=False)
        mouse.smooth_move(aion.x+ p[0] + 100, aion.y + p[1] + 60)
        mouse.click()

        # craft all
        mouse.smooth_move(aion.x+ p[0] + 270, aion.y + p[1] + 325)
        aion.chat.readlines()
        sleep(0.5)
        mouse.click()
        print 'Wykonuje zadanie'

        #to_do = 2
        done_count = 0
        bad = 0
        last_msg_len = 0
        while done_count < to_do:
            aion.chat.readlines()
            crafted, status_skill_points = aion.chat.crafted()
            if status_skill_points:
                print '--==|| {0} ||==--          '.format(status_skill_points)
                if int(status_skill_points) % 100 == 99:
                    self.do_learn = True
            if crafted == 'successfully':
                done_count += 1
                msg = 'Wykonano {0} z {1} (sukces)'.format(done_count, to_do)
                last_msg_len = len(msg)
                print msg + ('\r' * last_msg_len),

            elif crafted == 'failed':
                done_count += 1
                bad += 1
                msg = 'Wykonano {0} z {1} (porazka)'.format(done_count, to_do)
                last_msg_len = len(msg)
                print msg + ('\r' * last_msg_len),

            sleep(1)
        print ''
        # hide craft tools window
        sleep(1)
        key.tap(key.K_ESCAPE)


class Cooking(Craft):
    def __init__(self):
        super(Cooking, self).__init__()
        self.craft = 'cooking'
        self.master_name = 'Hestia'
        self.supplies_name = 'Luelas'
        self.work_quest_pos = -40
        self.supplies_in_items = ['Verteron Pepper']
        self.supplies = ([
            'Cooking Enhancement Stone', 'Salt',
            'Sugar', 'Abex Butter',
            'Brandy', 'Qooqoo Egg',
            'Storage Container', 'Cheese',
            'Cream', 'Ulmus Oil',

            'Omblic Wine', 'Mela Vinegar',
            'Hot Spice', 'Kukuru Powder',
            'Triora Noodle', 'Curry Powder',
            'Natural Water', 'Soy Sauce',
            'Bay Salt', 'White Sugar',

            'Meon Powder', 'Ormea Oil',
            'Tange Powder', 'Ervio Oil',
            'Nenana Powder', 'Beshu Oil',
            'Unifying Powder', 'Mixed Spices']
            )

class Alchemy(Craft):
    def __init__(self):
        super(Alchemy, self).__init__()
        self.craft = 'alchemy'
        self.master_name = 'Diana'
        self.supplies_name = 'Darius'
        self.work_quest_pos = -70
        self.supplies_in_items = []
        self.supplies = ([
            'Alchemy Enhancement Stone', 'Lesser Catalyst',
            'Betua Paper', 'Dried Gumi Tail',
            'Glass Bottle', 'Crynac Scale',
            'Larail Tears', 'Scorpin Poison Stinger',
            'Reaction Activator', 'Solvent',

            'Alcohol', 'Distilled Water',
            'Minor Red Squaro', 'Minor Red Pellan',
            'Catalyst', 'Koa Paper',
            'Lesser Red Squaro', 'Lesser Red Pellan',
            'Red Squaro', 'Red Pellan',

            'Greater Catalyst', 'Greater Red Squaro',
            'Ulmus Paper', 'Abyss Dust',
            'Greater Red Pellan', 'Major Red Squaro',
            'Major Red Pellan', 'Egrasi Paper',
            'Major Catalyst', 'Fine Red Pellan',

            'Fine Red Squaro', 'Superb Red Squaro',
            'Superb Red Pellan', 'Premium Red Pellan',
            'Nanyu Paper', 'Fine Catalyst',
            'Premium Red Squaro', 'Special Greater Red Pellan',
            'Special Greater Red Squaro', 'Finest Red Pellan',

            'Finest Red Squaro', 'Sublime Crystal Activator',
            'Sublime Acid', 'Sublime Potion Bottle',
            'Sublime Catalyst', 'Pirian Paper',
            'Sublime Spiritual Water', 'Balic Hobby Knife',
            'Balaur Synthetic Agent']
            )


if __name__ == "__main__":
    db = DatabaseNet()
    aion = Aion()
    aion.run()
    aion.to_front()

    craft = Cooking()
    #craft = Alchemy()

    rounds = 30
    for i in range(rounds):
        t1 = datetime.datetime.now()
        print 'Wykonuje prace: {0}/{1}'.format(i+1, rounds)
        print 'Biore zadanie'
        craft.speak_to_master()
        craft.work()
        #aion.to_front()
        craft.speak_to_master()
        craft.work_done()
        t2 = datetime.datetime.now()
        dt = t2 - t1
        s = dt.seconds % 60
        m = dt.seconds / 60
        print 'czas wykonania jednego: {0}m {1}s'.format(m, s)
        s = dt.seconds*(rounds-i-1)
        m = s / 60
        s = s % 60
        print 'czas wykonania pozostalych: {0}m {1}s'.format(m, s)
        if craft.do_learn:
            craft.speak_to_master()
            craft.learn()
            craft.do_learn = False


    # You do not have enough Kinah to buy the item.
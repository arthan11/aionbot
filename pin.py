import sys
import Image
from time import sleep
from os import remove
from ssim import compute_ssim
from autopy import bitmap, key, mouse, color


class Pin(object):
    def __init__(self, aion=None):
        self.aion = aion
        self.rows = []
        self.digits = {}
        for i in range(10):
            img = Image.open('img_hd/pin/pin_{0}.png'.format(i))
            is_yellow = (i%2 == 1)
            self.digits[str(i)] = {'img': img, 'is_yellow': is_yellow}
        self.digits['B'] = {'img': Image.open('img_hd/pin/pin_B.png'), 'is_yellow': True}
        self.digits['C'] = {'img': Image.open('img_hd/pin/pin_C.png'), 'is_yellow': True}

    def crop(self, digit):
        # przycina obrazek do rozmiaru znaku na nim
        yellow = None
        x_min = digit.width
        x_max = 0
        y_min = digit.height
        y_max = 0
        for y in range(digit.height):
            for x in range(digit.width):
                c = digit.get_color(x, y)
                r, g, b = color.hex_to_rgb(c)
                if r > 200:
                    if x < x_min:
                        x_min = x
                    if x > x_max:
                        x_max = x
                    if y < y_min:
                        y_min = y
                    if y > y_max:
                        y_max = y

                    if yellow == None:
                        yellow = b < 200

        w = x_max - x_min
        h = y_max - y_min
        #print x_min, x_max, y_min, y_max
        return digit.get_portion((x_min, y_min), (w, h)), yellow

    def recognize_digit(self, img, is_yellow):
        best_match = None
        best_match_ssim = None
        for key in self.digits.keys():
            if self.digits[key]['is_yellow'] != is_yellow:
                continue
            ssim = compute_ssim(img, self.digits[key]['img'])
            if best_match_ssim == None:
                best_match_ssim = ssim
                best_match = key
            else:
                if (ssim > best_match_ssim):
                    best_match_ssim = ssim
                    best_match = key
        return best_match

    def div_img(self, img):
        #proc_x = float(img.width) / 1920.0
        #proc_y = float(img.height) / 1080.0
        #print proc_x, proc_y
        #x, y, w, h = 1058, 468, 141, 176
        #print x, y, w, h

        '''
        x = int(0.551042 * float(img.width))
        y = int(0.433334 * float(img.height))
        w = int(proc_x * float(w))
        h = int(proc_y * float(h))
        '''
        x, y, w, h = self.x, self.y, 78, 96
        #print x, y, w, h

        scr = img.get_portion((x, y), (w, h))
        #scr.save('test_small.png')
        #sys.exit()
        w = w / 3
        h = h / 4
        rows = []
        for y in range(4):
            row = []
            for x in range(3):
                name = 'pin_{0}_{1}.png'.format(x, y)
                #print name
                digit = scr.get_portion((x*w, y*h), (w, h))
                digit, is_yellow = self.crop(digit)
                digit.save(name)
                img = Image.open(name)
                d = self.recognize_digit(img, is_yellow)
                remove(name)
                row.append(d)
            rows.append(row)
        return rows

    def get_digit_cord(self, digit):
        digit = str(digit)
        for j in range(3):
            for i in range(4):
                if self.rows[i][j] == digit:
                    return j, i
        return None

    def find_degits(self, img, pos_b):
        try:
            self.x = pos_b[0] - 61
            self.y = pos_b[1] - 86
            self.rows = self.div_img(img)
            for i in range(10):
                cord = self.get_digit_cord(i)
                #print i, cord
                if cord == None:
                  return False
            return True
        except:
            return False

    def move_to_digit(self, digit):
        x, y = self.get_digit_cord(digit)
        w = 78.0 / 3.0
        h = 96.0 / 4.0
        x = int(0.5 * w + w * float(x))
        y = int(0.5 * h + h * float(y))
        #print x, y

        x = self.aion.x + self.x + x
        y = self.aion.y + self.y + y
        #print x, y
        mouse.smooth_move(x, y)
        mouse.click()

    def write(self, pin):
        if self.aion == None:
            print 'nie przekazano obiektu Aion'
            sys.exit()
        for digit in pin:
            #print digit
            self.move_to_digit(digit)
        key.tap(key.K_RETURN)

if __name__ == "__main__":
    #scr = bitmap.Bitmap.open('pin_2.png')
    scr = bitmap.Bitmap.open('screen.png')

    pin = Pin()
    tab = pin.div_img(scr)
    for row in tab:
        print row



# -*- coding: utf-8 -*-
import urllib2
import re
import json
import sys


class Item(object):
    def __init__(self):
        self.id = ''
        self.title = ''
        self.img_url = ''

class Tip(object):
    def __init__(self):
        self.item = Item()
        self.materials = []

    def material_by_id(self, id):
        for material in self.materials:
            if material['item'].id == id:
                return material
    def material_by_title(self, title):
        for material in self.materials:
            if material['item'].title == title:
                return material

class DatabaseNet(object):
    def __init__(self):
        self.base_url = 'http://aiondatabase.net/'
        self.aiondatabase_com_url = 'http://www.aiondatabase.com/'

    def get_url(self, url):
        response = urllib2.urlopen(url)
        src = response.read()
        return src

    def get_name_from_datebase_com(self, type, id):
        #http://www.aiondatabase.com/recipe/155004206
        url = '{0}{1}/{2}'.format(self.aiondatabase_com_url, type, id)
        src = self.get_url(url)
        if src:
            rows = re.compile('<td>&nbsp;(.+?)</td>', re.M|re.DOTALL).findall(src)
            if rows:
                return rows[0]

    def get_id_by_name(self, a, type, name):
        #'aiondatabase.net/query.php?a=recipes&type=order&l=us'
        url = '{0}query.php?a={1}&type={2}&l=us'.format(self.base_url, a, type)
        src = self.get_url(url)
        items = json.loads(src)['aaData']
        #print name
        for item in items:
            rows = re.compile('data-id="item--(.+?)"><b>(.+?)</b></a></div>', re.M|re.DOTALL).findall(item[1])
            if rows:
                if rows[0][1] == name:
                    return rows[0][0]

    def get_tip(self, type, id):
        url = '{0}tip.php?id={1}--{2}&l=us&nf=on'.format(self.base_url, type, id)
        src = self.get_url(url)
        if not src:
            #print 'nie znaleziono {0}!'.format(id)
            name = self.get_name_from_datebase_com('recipe', id)
            if name:
                #print name
                id2 = self.get_id_by_name('recipes', 'order', name)
                if id2:
                    return self.get_tip(type, id2)

        tip = Tip()
        tip.item.id = id
        rows = re.compile('class="item_title"><b>(.+?)</b>', re.M|re.DOTALL).findall(src)
        if rows:
            tip.item.title = rows[0]

        rows = re.compile('<div>Required Materials<br>(.+?)</div>', re.M|re.DOTALL).findall(src)
        if rows:
            mat_src = re.compile('data-id="item--(.+?)" class="(.+?)">(.+?)</a>', re.M|re.DOTALL).findall(rows[0])
            for i, mat in enumerate(mat_src):
                if i%2 == 0:
                    material = {'item': Item()}
                    material['item'].id = mat[0]
                    tip.materials.append(material)
                    img = mat[2]
                    img = re.compile('<img src="(.+?)"', re.M|re.DOTALL).findall(img)
                    material['item'].img_url = img[0]
                else:
                    title = mat[2]
                    count = int(title[title.find('(')+1:-1])
                    title = title[:title.find('(')-1]
                    material['item'].title = title
                    material['count'] = count


        return tip


if __name__ == "__main__":
    db = DatabaseNet()
    tip = db.get_tip('item', '152204226')
    print tip.item.id, tip.item.title
    if tip.materials:
        print 'Required Materials'
        for material in tip.materials:
            print material['item'].id
            print material['item'].title
            print material['count']
            print material['item'].img_url
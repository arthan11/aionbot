# -*- coding: utf-8 -*-
import sys
import re
import ctypes
import subprocess
import win32api
import win32gui
import win32con
from time import sleep
from autopy import bitmap, key, mouse, color
from pin import Pin

#chat_filename = 'd:\\temp\\NCSOFT\\Aion\\Chat.log'
chat_filename = 'e:\\Temp\\NCSOFT\\Aion\\Chat.log'


class ChatLog(object):
    def __init__(self):
        self.file = None
        self.lines = ''

    def open(self, filename):
        self.file = open(filename, 'r')
        self.file.readlines()
        self.lines = ''

    def readlines(self):
        if self.file:
            self.lines = self.file.readlines()
        else:
            self.lines = ''
        return self.lines

    def defeated(self):
        if len(self.lines) > 0:
            for line in self.lines:
                #print line
                rows = re.compile('You have gained (.+?) XP from (.+?)\.',re.M|re.DOTALL).findall(line)
                if rows:
                    print rows[0][1]
                    return True
            return False

    def learned_recipe(self):
        if len(self.lines) > 0:
            for line in self.lines:
                #print line
                rows = re.compile('You have learned \[recipe_ex:(.+?);.',re.M|re.DOTALL).findall(line)
                if rows:
                    return rows[0]
            return None

    def acquired_items(self):
        items = []
        if len(self.lines) > 0:
            for line in self.lines:
                #print line
                rows = re.compile('You have acquired (.+?) \[item:(.+?);',re.M|re.DOTALL).findall(line)
                if rows:
                    #return rows[0][1], rows[0][0]
                    items.append([rows[0][1], rows[0][0]])
            return items

    def quest_complete(self):
        if len(self.lines) > 0:
            for line in self.lines:
                #print line
                if line.find('Quest complete') > 0:
                    return True

    def crafted(self):
        status = None
        skill_point_info = None
        if len(self.lines) > 0:
            for line in self.lines:
                #print line
                rows = re.compile('Your (.+?) skill has been upgraded to (.+?) points.', re.M|re.DOTALL).findall(line)
                if rows:
                    skill_point_info = rows[0][1]
                if line.find('You have crafted successfully.') > 0:
                    status = 'successfully'
                if line.find('You have failed to craft') > 0:
                    status = 'failed'
        return status, skill_point_info

class Aion(object):
    def __init__(self):
        self.hwnd = 0
        self.rect = [0, 0, 0, 0]
        self.width = 0
        self.height = 0
        self.x = 0
        self.y = 0
        self.make_screenshot = True
        self.screen = None
        self.chat = ChatLog()
        self.chat.open(chat_filename)

    def ToggleShift(self, state):
        if state:
            win32api.keybd_event(win32con.SHIFT_PRESSED, 0, win32con.KEYEVENTF_EXTENDEDKEY, 0)
        else:
            win32api.keybd_event(win32con.SHIFT_PRESSED, 0, win32con.KEYEVENTF_EXTENDEDKEY|win32con.KEYEVENTF_KEYUP, 0)

    def close_game_launcher(self):
        hwnd = win32gui.FindWindow(None, "Game Launcher")
        if hwnd > 0:
            win32gui.SendMessage(hwnd, win32con.WM_CLOSE, 0, 0)

    def get_hwnd(self):
        #self.hwnd = win32gui.FindWindow(None, "AION Client")
        self.hwnd = win32gui.FindWindow(None, "AION Client (64bit)")
        return self.hwnd

    def get_rect(self):
        self.rect = [0, 0, 0, 0]
        self.width = 0
        self.height = 0
        self.x = 0
        self.y = 0
        if self.hwnd > 0:
            self.rect = win32gui.GetWindowRect(self.hwnd)
            self.x = self.rect[0]
            self.y = self.rect[1]
            self.width = self.rect[2] - self.x
            self.height = self.rect[3] - self.y

    def get_screen(self):
        if (self.make_screenshot) or (self.screen == None):
            bmp = bitmap.capture_screen()
            self.screen = bmp.get_portion((self.x, self.y), (self.width, self.height))
        return self.screen

    def where_is_img_on_screen(self, img, tolerance=0.0):
        #print img
        small = bitmap.Bitmap.open(img)
        big = self.get_screen()
        return big.find_bitmap(small, tolerance)

    def is_img_on_screen(self, img):
        pos = self.where_is_img_on_screen(img)
        return pos != None

    def login(self, user, pas):
        self.wait_for_img('login_button_OK.bmp')
        print 'loguje...'
        for s in user:
            if s not in ['@', '_']:
                key.tap(s)
            else:
                key.tap(s, key.MOD_SHIFT)
        key.tap(9)
        key.type_string(pas)
        key.tap(key.K_RETURN)
        sys.exit()
        print 'akceptuje licencje...'
        self.wait_for_img('login_button_accept.bmp')
        key.tap(key.K_RETURN)
        print 'zalogowany'

    def wait_for_img(self, img, delay=5, tolerance=0.0, print_msg=True):
        while True:
            pos = self.where_is_img_on_screen(img)
            if pos != None:
                return pos
            if print_msg:
                print 'czekam...'
            sleep(delay)

    def run(self):
        self.get_hwnd()
        if self.hwnd == 0:
            print 'uruchamiam Aiona...'
            subprocess.Popen(['aion_as_admin.bat'])
            print 'czekam na okno logowania...'
            sleep(120)
        else:
            print 'Aion uruchomiony'
        self.get_hwnd()
        self.get_rect()
        self.close_game_launcher()

    def to_front(self, sleep_time=1):
        win32gui.SetForegroundWindow(self.hwnd)
        mouse.smooth_move(self.x + self.width/2, self.y + self.height/2)
        sleep(sleep_time)

    def chose_character(self, number, pin=''):
        self.wait_for_img('login_button_select_server.bmp')

        char_pos = float(self.width) / 8 * (number - 0.5)
        x = self.x + char_pos
        y = self.y + 0.85 * float(self.height)
        x = int(x)
        y = int(y)
        mouse.smooth_move(x, y)
        mouse.click()
        sleep(0.5)
        mouse.click()

        if pin != '':
            #sleep(5)
            pos_b = self.wait_for_img('img_hd/pin/pin_B.png')
            p = Pin(self)
            self.screen = None
            scr = self.get_screen()
            if not p.find_degits(scr, pos_b):
                print 'nie udalo sie rozpoznac pol pinu'
                sys.exit()
            p.write(pin)


    def chose_system_menu(self, number):
        option_pos = 305 + 20 * number
        x = self.x + 0.5 * float(self.width)
        y = self.y + option_pos
        x = int(x)
        y = int(y)
        mouse.smooth_move(x, y)
        mouse.click()

    def logout(self):
        key.tap('o')
        sleep(2)
        self.chose_system_menu(2)

    def quit(self):
        key.tap('o')
        sleep(2)
        self.chose_system_menu(1)

    def characters(self):
        key.tap('o')
        sleep(2)
        self.chose_system_menu(1)

    def get_my_hp(self):
        big = self.get_screen()
        x = 103
        y = 578
        w = 108
        h = 8
        my_hp = big.get_portion((x, y), (w, h))
        hp = 0
        for i in range(my_hp.width):
            c = my_hp.get_color(i, 0)
            r = color.hex_to_rgb(c)[0]
            if r > 125:
                hp += 1
            else:
                break
        hp = float(hp) / 108.0 * 100
        hp = round(hp, 2)
        return hp

    def get_my_mp(self):
        big = self.get_screen()
        x = 103
        y = 589
        w = 108
        h = 8
        my_mp = big.get_portion((x, y), (w, h))
        #my_mp.save('mp.png')
        mp = 0
        for i in range(my_mp.width):
            c = my_mp.get_color(i, 0)
            b = color.hex_to_rgb(c)[2]
            if b > 125:
                mp += 1
            else:
                break
        mp = float(mp) / 108.0 * 100
        mp = round(mp, 2)
        return mp

if __name__ == '__main__':
    pass